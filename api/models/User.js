/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt-nodejs');
module.exports = {

    types: {
        is_email: function(email){
            return require("email-validator").validate(email);
        }
    },
//    attributes: {
//        name: {
//            type: 'string',
//            require: true
//        },
//        email: {
//            type: 'integer',
//            is_email: true
//        },
//        goods: {
//            required: false,
//            collection: 'Goods',
//            via: 'owner'
//        }
//    }

    //adapter: 'mongo',

    attributes: {
        provider: 'STRING',
        uid: {
            type: 'STRING',
            //autoIncrement: true
            //unique: true
        },
        name: 'STRING',
        identifier: {
            type: 'is_email',
            required: true,
            unique: true
        },
        isAdmin: {
            type: 'boolean',
            defaultsTo: false
        },
        password: {
            type: 'string',
            required: true
        },
        lastname: 'STRING'
    },
    
    validationMessages: {
        identifier: {
            is_email: 'не верный формат e-mail',
            required: 'введите e-mail',
            unique: 'такой e-mail уже есть в системе',
        },
        password: {
            required: 'введите пароль',
        }
    },
    
    //Override toJSON method to remove password from API
    toJSON: function() {
        var obj = this.toObject();
        // Remove the password object value
        delete obj.password;
        // return the new object without password
        return obj;
    },
    
    
    beforeCreate: function(user, cb) {
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(user.password, salt, function(){},  function(err, hash) {
                if (err) {
                    console.log(err);
                    cb(err);
                }else{
                    user.password = hash;
                    cb(null, user);
                }
            });
        });
    },
    
    getIsAdmin: function(){
        return this.isAdmin ? true : false;
    },
    
    getAdmin: function(){
        return this.isAdmin ? true : false;
    },
    
    signUp: function(userObj, call){
        User.create(userObj, function(err, user){
            //todo email, sms с паролем
            call(err, user);
        });
    } 
};

