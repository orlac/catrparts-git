'use strict';
var async = require('async');
var mongoose = require('mongoose');
module.exports = exports = function slugPlugin(schema, options) {


  var _save_nodes = function(nodes, parentId, cb){
      async.map(nodes, 
        function(item, _cb){
          if(parentId){
            item.parentId = parentId;
          }else{
            item.parentId = null;
          }
          if(item.isDeleted){

            if(item._id){
              var model = mongoose.model(options.collection, schema);
              model.Remove({_id: item._id}, function(err, m){

                if(err){
                  cb(err);
                  return;
                }

              })
            }

          }else{
            _save_node(item, function(err, _id){
                if(err){
                  cb(err);
                  return;
                }
                item.children = item.children || [];
                if(item.children.length > 0){

                  _save_nodes(item.children, _id, function(err, res){
                    
                    if(err){
                      cb(err);
                      return;
                    }else{
                      cb(null, item);
                    }

                  });
                  
                }else{
                  cb(null, item);
                }
            });  
          }
          
        },
        function(err, results){
         
        });
  }

  var _save_node = function(data, cb){
      var model = mongoose.model(options.collection, schema); 
      var _id = data._id || '';
      delete(data._id);
      if(_id){
        model.update({_id: _id}, data, {
          upsert: true
        }, function (err, mymodel) {
          if(err){
            cb(err);
            return;
          }
          cb(err, _id);
        });
      }else{
        model = model(data);
        model.save(function (err, mymodel) {
          if(err){
            cb(err);
            return;
          }
          cb(err, mymodel._id);
        });
      }
  }

  schema.static('saveFromArray', function (data, callback) {

    _save_nodes(data, null, callback);

  });

  


};