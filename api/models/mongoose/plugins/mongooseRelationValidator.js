'use strict';

var _exist = function (model) {

	return function(value, respond){
		if(value){
			var promise = model.findOne({_id: value}).exec();
		    promise.then( function(data){
				if(!data){
					respond(false);  
				}
				respond(true);
		    }, function(err){
		    	throw err;
		    });		
		}else{
			respond(true);  
		}
	}
	
}

module.exports.exist = _exist;