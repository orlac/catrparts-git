var autoIncrement = require('mongoose-auto-increment');
var validators = require('mongoose-validators');
var relationship = require("mongoose-relationship");
var materializedPlugin = require('mongoose-materialized');
var materializedSavePlugin = require('./plugins/mongooseMaterializedSave.js');

var _cl = 'CategoryParts';

var _fn = function(mongoose){

	var schema = new mongoose.Schema({
	    name: {
	    	type: String,
	    	index: true,
	    	validate: [validators.isLength({message: 'название не меньше 2 и не больше 50 симв.' }, 2, 50)]
	    },
	}, { collection: _cl });

	schema.path('name').required(true, 'укажите название');

	/** validators */

	/** get */
	schema.get('toJSON', { virtuals: true, getters: true })
	schema.get('toObject', { virtuals: true, getters: true })
	 

	/** autoincrement */ 
	schema.plugin(autoIncrement.plugin, {
	    model: _cl,
	    field: 'mid',
	}); 
	

	/** tree */
	schema.plugin(materializedPlugin);
	schema.plugin(materializedSavePlugin, { collection: _cl } );

	return mongoose.model(_cl, schema);
}

module.exports = _fn;