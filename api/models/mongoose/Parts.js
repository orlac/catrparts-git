'use strict';
var autoIncrement = require('mongoose-auto-increment');
//var relationship = require("mongoose-relationship");
var validators = require('mongoose-validators');
var mongooseRelationValidator = require('./plugins/mongooseRelationValidator.js');

var _fn = function(mongoose){

	var schema = new mongoose.Schema({
	    
	    name: {
	    	type: String,
	    	index: true,
	    	validate: [validators.isLength({message: 'навание не больше 250 симв.' }, 0, 250)]
	    },
	    description: {
	    	type: String,
	    	validate: [validators.isLength({message: 'описание не больше 500 симв.' }, 0, 500)]
	    },
	    price: {
	    	type: Number,	
	    	validate: [validators.isFloat()]
	    	
	    },
	    myPrice: {
	    	type: Number,	
	    	validate: [validators.isFloat()]
	    },
	    numPart: {
	    	type: String,
	    	index: true,
	    },
	    myNumPart: {
	    	type: String,
	    	index: true,
	    },
	    extParam: {
	    	type: String,
	    	index: true,
	    },
	    numPart2: {
	    	type: String,
	    	index: true,
	    },
	    active: {
	    	type: Boolean,
	    },
	    source: {
	    	type : mongoose.Schema.ObjectId, 
	    	ref: 'SourcePart'
	    }
	}, { collection: 'Parts' } );


	schema.path('name').required(true, 'укажите название');
	//schema.path('source').validate(mongooseRelationValidator.exist(db.SourcePart), '{PATH} failed validation. производитель не найдена');


	/** get */
	schema.get('toJSON', { virtuals: true, getters: true })
	schema.get('toObject', { virtuals: true, getters: true })
	 

	schema.plugin(autoIncrement.plugin, {
	    model: 'Parts',
	    field: 'mid',
	});


	schema.statics.saveData = function (data, cb) {
		//TODO use plugins
		data.source = data.source || {};
		data.source = data.source._id;

		//var model = mongoose.model('SourcePart', schema)(data);	
		var model = mongoose.model('Parts', schema);	
		var _id = data._id || '';
		delete(data._id);
		if(_id){
			model.update({_id: _id}, data, {
				upsert: true
			}, function (err, mymodel) {
		 		cb(err, mymodel);
			});
		}else{
			model = model(data);
			model.save(function (err, mymodel) {
				cb(err, mymodel);
			});
		}
	}


	return mongoose.model('Parts', schema)

}

module.exports = _fn;