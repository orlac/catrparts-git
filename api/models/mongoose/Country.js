var autoIncrement = require('mongoose-auto-increment');
var validators = require('mongoose-validators');
var relationship = require("mongoose-relationship");

var _fn = function(mongoose){

	var schema = new mongoose.Schema({
	    name: {
	    	type: String,
	    	index: true,
	    	validate: [validators.isLength({message: 'навание не меньше 2 и не больше 50 симв.' }, 2, 50)]
	    },
	    // sourceParts:[{ 
	    // 	type:mongoose.Schema.ObjectId, ref:"SourcePart" 
	    // }]
	}, { collection: 'Country' });

	schema.path('name').required(true, 'укажите название');

	/** validators */
	schema.path('name').index({ unique: true });
	//schema.path('name').validate([ validators.isLength({message: 'навание не меньше 2 и не больше 50 симв.' }, 2, 50)  ] );


	/** get */
	schema.get('toJSON', { virtuals: true, getters: true })
	schema.get('toObject', { virtuals: true, getters: true })
	 

	/** autoincrement */ 
	schema.plugin(autoIncrement.plugin, {
	    model: 'SourcePart',
	    field: 'mid',
	}); 
	return mongoose.model('Country', schema);

}

module.exports = _fn;