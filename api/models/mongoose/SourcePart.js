var autoIncrement = require('mongoose-auto-increment');
var relationship = require("mongoose-relationship");

var _fn = function(mongoose){


	

	/** validators */

	var _maxLength = function(len){
		return function(val){
			if(val){
				return val.toString().length <= len;	
			}
			return true;
		}
	}

	var _minLength = function(len){
		return function(val){
			if(val){
				return val.toString().length >= len;	
			}
			return true;
		}
	}

	var schema = new mongoose.Schema({
	    name: {
	    	type: String,
	    	index: true,
	    },
	    description: {
	    	type: String,
	    },
	    // countryId: {
	    // 	type: String,
	    // },
	    logo: {
	    	type: String,
	    },
	    active: {
	    	type: Boolean,
	    },
	    country: {
	    	type : mongoose.Schema.ObjectId, 
	    	ref: 'Country'
	    }
	    // country: { 
	    // 	type : mongoose.Schema.ObjectId, 
	    // 	ref:"Country", 
	    // 	childPath:"sourceParts" 
	    // }
	}, { collection: 'SourcePart' } );


	schema.path('name').required(true, 'укажите название');

	/** validators */
	schema.path('name').index({ unique: true });
	schema.path('name').validate( _maxLength(50), 'навание не ьольше 50 симв.' );
	schema.path('name').validate( _minLength(2), 'навание не меньше 2 симв.' );
	schema.path('description').validate( _maxLength(500), 'навание не ьольше 500 симв.');
	schema.path('country').validate(function (value, respond) {
		if(value){
			var promise = db.Country.findOne({_id: value}).exec();
		    promise.then( function(data){
				if(!data){
					respond(false);  
				}
				respond(true);
		    }, function(err){
		    	throw err;
		    });		
		}else{
			respond(true);  
		}
	}, '{PATH} failed validation. страна не найдена');


	/** get */
	schema.get('toJSON', { virtuals: true, getters: true })
	schema.get('toObject', { virtuals: true, getters: true })
	 

	/** virtuals */
	// schema.virtual("country").get(function() {
	//     return this.country;
	// }).set(function(value) {
	//     this.country = value;
	//     this.countryId = value;
	// });

	schema.plugin(autoIncrement.plugin, {
	    model: 'SourcePart',
	    field: 'mid',
	}); 
	//schema.plugin(relationship, { relationshipPathName:['country'] });


	schema.statics.saveData = function (data, cb) {
		data.country = data.country || {};
		data.country = data.country._id;

		//var model = mongoose.model('SourcePart', schema)(data);	
		var model = mongoose.model('SourcePart', schema);	
		var _id = data._id || '';
		delete(data._id);
		if(_id){
			model.update({_id: _id}, data, {
				upsert: true
			}, function (err, mymodel) {
		 		cb(err, mymodel);
			});
		}else{
			model = model(data);
			model.save(function (err, mymodel) {
				cb(err, mymodel);
			});
		}
		

		// if(data._id){
		// 	model.update(function (err, mymodel) {
		// 		cb(err, mymodel);
		// 	});
		// }else{
		// 	model.save(function (err, mymodel) {
		// 		cb(err, mymodel);
		// 	});
		// }

		
	  	
		
	}

	return mongoose.model('SourcePart', schema)

}

module.exports = _fn;