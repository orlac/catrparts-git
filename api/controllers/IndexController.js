/**
 * IndexController
 *
 * @description :: Server-side logic for managing indices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
    index: function(req, res){
        console.log('req.user', req.user);
        return res.view( {req: req} );
    },

    menu: function(req, res){
    	//TODO куда-нибудь в настройки
    	var menu = [
                {
	                label: 'Главная'
	                ,url: '/'
	                ,state: 'home'
	                //,ico: 'fa fa-truck '
	            }
	            ,{
	                label: 'О компании'
	                ,url: '/about'
	                ,state: 'about'
	                //,ico: 'fa fa-cogs'
	            }
	            ,{
	                label: 'Помощь'
	                ,url: '/help'
	                ,state: 'help'
	                //,ico: 'fa fa-barcode'
	            }
	            ,{
	                label: 'Каталог'
	                ,url: '/parts'
	                ,state: 'parts'
	                //,ico: 'fa fa-barcode'
	            }
            ];

        if(req.user){
        	if(RbacService.getIsAdmin(req.user)){
        		menu.push({
	                label: 'Админка'
	                ,url: '/admin/index'
	            });

        	}else{
        		menu.push({
	                label: 'Личный кабинет'
	                ,url: '/cabinet'
	                ,state: 'cabinet'
	            });	
        	}

        	menu.push({
                label: 'Выход'
                ,url: '/logout'
            });

        }else{
			menu.push({
                label: 'Войти'
                ,url: '/signin'
                ,state: 'signin'
                //,ico: 'fa fa-thumbs-up'
            });
            menu.push({
                label: 'Регистрация'
                ,url: '/signup'
                ,state: 'signup'
                //,ico: 'fa fa-thumbs-up'
            });        	
        }

    	return res.json({
    		success: true,
    		data: menu
    	});	
    }
    
};

