/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require("passport");
module.exports = {
	
//    login: function(req, res){
//        //var err_info={};
//        res.view("auth/login");
//    },
    
    /** sign in */
    
    signin: function(req, res){
        res.view("auth/signin");
    },
    
    signinProcess: function(req, res){
        passport.authenticate('local', function(err, user, info) {
            if ((err) || (!user)) {
                res.send(403, { success: false, error: info.message,   info: err  });
            }else{
                req.logIn(user, function(err){
                    if (err){
                        err.invalidAttributes = require('sails-validation-messages')(User, err.invalidAttributes);
                        res.send(err.status, { success: false, error: ViewErrorService.toString(err),   info: err  });
                    }else{
                        res.send({ success: true, user: user });
                    } 
                });
            }
        })(req, res, function(err, user, info){
            res.send({ success: false, err: err, errinfo: info, user: user });
        });
    },
    
    
    /** sign up */
    
    signup: function(req, res){
        res.view("auth/signup");
    },
    
    signupProcess: function(req, res){

        req.body.user=req.body.user || {};
        User.signUp( { 
            identifier: req.body.user.identifier, 
            password: req.body.user.password },  function(err, user){
                if(err){
                    err.invalidAttributes = require('sails-validation-messages')(User, err.invalidAttributes);
                    res.send(err.status, { success: false, error: ViewErrorService.toString(err),   info: err  });
                    //res.negotiate(err);
                }else{
                    res.send({ success: true });
                }
        } );
    },
    
    logout: function (req,res){
        req.logout();
        return res.redirect('/');
        //res.send('logout successful');
    }
};

