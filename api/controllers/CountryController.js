/**
 * CountryController
 *
 * @description :: Server-side logic for managing countries
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `CountryController.listData()`
   */
  listData: function (req, res) {
    var promise = db.Country.find().exec();
    promise.then( function(data){
          return res.json({
              success: true,
              data: data
          }); 
      }, function(err){
          return res.json(400, { success: false, error: err  });
      });
  }
};

