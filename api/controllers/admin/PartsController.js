/**
 * Admin/SourcePartController
 *
 * @description :: Server-side logic for managing admin/sourceparts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  save: function(req, res) {
      var data = req.body;
      //data.country = '546278c86a498e3d418f92ce';
      var model = db.Parts( data );

      db.Parts.saveData(data, function (err, mymodel) {
        if (err){ 
          return res.json(400, { 
            success: false, 
            error: ViewErrorService.toString(err) ,
            info: err 
          });
        } 
        return res.json({
            success: true,
            model: mymodel
        });
      });

  },

  listData: function (req, res) {

    var curPage = Math.max(0, req.query.page) || 0;
    var size = 100;

    var query = db.Parts.find().populate('source');
    query.paginate(curPage+1, size, function(err, docs, count) {
      if(err){
        return res.json(400, { success: false, error: err  });
      }else{
        return res.json({
            success: true,
            data: docs,
            pager: {
              count: count,
              curPage: curPage,
              pageSize: size,
              countPages: Math.ceil(count/size)
            }
            
        }); 
      }
    });

  },

  get: function (req, res) {
    var mid = req.query.mid;
    var promise = db.Parts.findOne({mid: mid}).populate('source').exec();

    promise.then( function(data){
      if(!data){
        return res.json(404);  
      }
      return res.json({
          success: true,
          data: data
      }); 
    }, function(err){
        return res.json(400, { success: false, error: err  });
    });
  },

  delete: function (req, res) {
    var _id = req.query._id;
    var promise = db.Parts.findOneAndRemove({_id: _id}).exec();

    promise.then( function(data){
      if(!data){
        return res.json(404);  
      }
      return res.json({
          success: true,
          data: data
      }); 
    }, function(err){
        return res.json(400, { success: false, error: err  });
    });
  },


  import: function(req, res){
    req.file('file').upload(function (err, files) {
      if (err){
        return res.serverError(err);
      }

      var xlsx = require('node-xlsx');
      var async = require('async');

      var file = files[0];
      var obj = xlsx.parse(file.fd);
      
      obj[0].data.shift();


      function _source(partData, sourceModel, _cb){
        if(!sourceModel){
          console.log('sourceModel', sourceModel);
        }
        var part = {
          name: partData[1],
          numPart: partData[0],
          source: (sourceModel)? sourceModel._id : null
        };
        var promise = db.Parts.findOne(part).exec();
        promise.then( function(partModel){

            partModel = partModel || {}
            partModel = partModel._doc || {};
            partModel.name=part.name;
            partModel.numPart=part.numPart;
            partModel.source=sourceModel;
            partModel.price=parseFloat(partData[7]);
            partModel.myPrice=parseFloat(partData[8]);
            partModel.myNumPart=partData[10];
            partModel.extParam=partData[12];
            partModel.numPart2=partData[13];
            partModel.active=true;

            db.Parts.saveData(partModel, function (err, mymodel) {
              if(err){
                console.log('err', err);
              }
              _cb(err, mymodel);
            });
            
          }, function(err){
            _cb(err);
          });
      }

      async.map(obj[0].data, function(item, cb){
        //производитель
        var numPart = item[0];
        var name = item[1];
        var sourceName = item[11];

        if(!sourceName){
          _source(item, null, function(err, pModel){
              if(err){
                console.log('err', err);
              }
              cb(err, pModel);
          });
        }else{
          var promise = db.SourcePart.findOne({name: sourceName}).exec();
          promise.then( function(model){

              if(model){
                _source(item, model._doc, function(err, pModel){
                  if(err){
                    console.log('err', err);
                  }
                  cb(err, pModel);
                });
              }else{

                var sourceData = {
                  name: sourceName
                };
                db.SourcePart.saveData(sourceData, function (err, sourceModel) {
                  if (err){ 
                    cb(err);
                  }else{
                    _source(item, sourceModel, function(err, pModel){
                      if(err){
                        console.log('err', err);
                      }
                      cb(err, pModel);
                    });
                  } 
                });
              }
              
            }, function(err){
              cb(err);
            });
        }
      }, function(err, results){
        if(err){
          return res.json(400, { 
            success: false, 
            error: err,
            info: err,
            results: results 
          });
        }else{
          return res.json({
            success: true,
            count: results.length
          });  
        }
      });
    });
  }  

};

