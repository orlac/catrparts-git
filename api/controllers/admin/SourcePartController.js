/**
 * Admin/SourcePartController
 *
 * @description :: Server-side logic for managing admin/sourceparts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `Admin/SourcePartController.index()`
   */
  index: function (req, res) {

    res.locals.layout = 'layouts/admin';
    return res.view( );
  	/*var sourcePart = db.SourcePart( {
  		name: 'fdbdfbdfbfd',
  		description: '4546546'
  	} );
  	sourcePart.save(function (err, mymodel) {
	    if (err){	
	    	return res.json(400, { success: false, error: err  });
	    } 
	    return res.json({
	      	model: mymodel
	    });
  	});

  	var stream = db.SourcePart.find().stream();
    	stream.on('data', function (doc) {
    		console.log('doc', doc);
  	  // do something with the mongoose document
  	}).on('error', function (err) {
  	  // handle the error
  	}).on('close', function () {
  	  // the stream is closed
  	});
*/
    /*return res.json({
      todo: 'index() is not implemented yet!'
    });*/
  },


  list: function (req, res) {

    res.locals.layout = false;
    return res.view( );

  },

  add: function (req, res) {

    res.locals.layout = false;
    return res.view(  );

  },

  edit: function (req, res) {
    res.locals.layout = false;
    return res.view(  );
  },

  save: function(req, res) {
      var data = req.body;
      //data.country = '546278c86a498e3d418f92ce';
      var sourcePart = db.SourcePart( data );

      db.SourcePart.saveData(data, function (err, mymodel) {
        if (err){ 
          return res.json(400, { 
            success: false, 
            error: ViewErrorService.toString(err) ,
            info: err 
          });
        } 
        return res.json({
            success: true,
            model: mymodel
        });
      });

  },

  listData: function (req, res) {

  	var promise = db.SourcePart.find().populate('country').exec();
    console.log('promise', promise);
    promise.then( function(data){
          return res.json({
              success: true,
              data: data
          }); 
      }, function(err){
          return res.json(400, { success: false, error: err  });
      });

  },

  get: function (req, res) {
    var mid = req.query.mid;
    var promise = db.SourcePart.findOne({mid: mid}).populate('country').exec();

    promise.then( function(data){
      if(!data){
        return res.json(404);  
      }
      return res.json({
          success: true,
          data: data
      }); 
    }, function(err){
        return res.json(400, { success: false, error: err  });
    });
  },

  delete: function (req, res) {
    var _id = req.query._id;
    var promise = db.SourcePart.findOneAndRemove({_id: _id}).exec();

    promise.then( function(data){
      if(!data){
        return res.json(404);  
      }
      return res.json({
          success: true,
          data: data
      }); 
    }, function(err){
        return res.json(400, { success: false, error: err  });
    });
  },



};

