/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module.exports = {
	
    index: function(req, res){

    	res.locals.layout = 'layouts/admin';

        console.log('res.user', res.user);
        return res.view( );
    },

    menu: function(req, res){
    	//TODO куда-нибудь в настройки
    	return res.json({
    		success: true,
    		data: [
    			// {
       //              label: 'главная'
       //              ,url: '/'
       //              ,state: 'index'
       //              ,ico: 'fa fa-home'
       //          },
                {
                    label: 'Модели'
                    ,url: '/catalog'
                    ,state: 'catalog'
                    ,ico: 'fa fa-truck '
                }
                ,{
                    label: 'Производители'
                    ,url: '/sourcePart'
                    ,state: 'sourcePart'
                    ,ico: 'fa fa-cogs'
                }
                ,{
                    label: 'Категории'
                    ,url: '/categoryParts'
                    ,state: 'categoryParts'
                    ,ico: 'fa fa-barcode'
                }
                ,{
                    label: 'Товары'
                    ,url: '/parts'
                    ,state: 'parts'
                    ,ico: 'fa fa-barcode'
                }
                ,{
                    label: 'Заказы'
                    ,url: '/orders'
                    ,state: 'orders'
                    ,ico: 'fa fa-thumbs-up'
                }
            ]
    	});	
    }
    
};
