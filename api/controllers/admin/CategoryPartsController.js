/**
 * Admin/CategoryPartsController
 *
 * @description :: Server-side logic for managing admin/categoryparts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `admin/CategoryPartsController.index()`
   */
  index: function (req, res) {
    res.locals.layout = 'layouts/admin';
    return res.view( );
  },

  save: function (req, res) {
    var data = req.body;
    data.tree = data.tree || [];

	db.CategoryParts.saveFromArray(data.tree, function (err) {
		if (err){ 
		  return res.json(400, { 
		    success: false, 
		    error: ViewErrorService.toString(err) ,
		    info: err 
		  });
		} 
		return res.json({
		    success: true,
		});
	});
  },

  remove: function(req, res) {
    var _id = data.body._id;
	db.CategoryParts.Remove({_id: _id}, function (err) {
		if (err){ 
		  return res.json(400, { 
		    success: false, 
		    error: ViewErrorService.toString(err) ,
		    info: err 
		  });
		} 
		return res.json({
		    success: true,
		});
	});
  },

  tree: function(req, res){
  	db.CategoryParts.GetFullArrayTree(function(err, data){
  		return res.json({
		    success: true,
		    tree: data
		});	
  	})
  }

  
};

