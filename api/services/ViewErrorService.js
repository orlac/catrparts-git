/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//TODO refactoring

var E_VALIDATION=function(obj){
    if( Object.keys(obj.invalidAttributes).length > 0 ){
        
        var str = [];
        for(var i in obj.invalidAttributes){
            for(var j in obj.invalidAttributes[i]){
                str.push(  obj.invalidAttributes[i][j].message );
            }
        }
        return str.join('\n');
        
    }else{
        return obj.summary;
    }
};

var E_MONGOOSE_VALIDATION=function(obj){
    if( Object.keys(obj.errors).length > 0 ){
        
        var str = [];
        for(var i in obj.errors){
            str.push(  obj.errors[i].message );
        }
        return str.join('\n');
        
    }else{
        return obj.message;
    }
};

var toString=function(obj){
    obj.invalidAttributes = obj.invalidAttributes || [];
    //validator = require('sails-validation-messages');
    if(Object.keys(obj.invalidAttributes).length > 0 ){
        return E_VALIDATION(obj);
    }

    if(obj.name === 'ValidationError'){
        return E_MONGOOSE_VALIDATION(obj);
    }
//    console.log('E_VALIDATION', obj.invalidAttributes);
//    switch(obj.error){
//        case 'E_VALIDATION':
//            return E_VALIDATION(obj);
//            break;
//    }
};



module.exports.toString=toString;