var async = require('async');
var scripts=[];
var css=[];

var addSript = function(src) {
	if(scripts.indexOf(src) < 0){
		scripts.push(src);	
	}
}

var getScripts=function(cb){
	cb(null, scripts);
	// async.map(scripts, 
	// 	function(item, _cb){
	// 		var _item = '<script src="'+item+'"></script>'
	// 		_cb(null, _item);
	// 	},
	// 	function(err, results){
	// 		if(err){
	// 			throw err;
	// 		}
	// 		results = results.join('\n');
	// 		cb(null, results);
	// 	});
}

var addCss = function(src) {
	if(css.indexOf(src) < 0){
		css.push(src);	
	}
}

var getCss=function(cb){
	cb(null, css);
	// async.map(css, 
	// 	function(item, _cb){
	// 		var _item = '<link rel="stylesheet" href="'+item+'">';
	// 		_cb(null, _item);
	// 	},
	// 	function(err, results){
	// 		if(err){
	// 			throw err;
	// 		}
	// 		results = results.join('\n');
	// 		cb(null, results);
	// 	});
}


module.exports.addSript=addSript;
module.exports.addCss=addCss;
module.exports.getScripts=getScripts;
module.exports.getCss=getCss;