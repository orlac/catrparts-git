 
/**
 * We load mongoose
 */
var mongoose = require('mongoose');
require('mongoose-pagination');
var config = require('../../config/connections');
var autoIncrement = require('mongoose-auto-increment');
 
//TODO use user/password here 
var _url = config.connections.someMongodbServer.host +':'+ config.connections.someMongodbServer.port +'/'+ config.connections.someMongodbServer.database;
// mongoose.connect('mongodb://localhost/MY_DATABASE');
mongoose.connect(_url);
 
/**
 * We check if the connection is ok
 * If so we will continue to load everything ...
 */
var db = mongoose.connection;
autoIncrement.initialize(db);
 
console.log('Try to connect to MongoDB via Mongoose ...');
 
db.on('error', console.error.bind(console, 'Mongoose connection error:'));
db.once('open', function callback() {
 
    console.log('Connected to MongoDB !');
 
});
 
/**
 * Let's make our Mongodb Schemas/Models
 */
module.exports = {

 	SourcePart: require('../models/mongoose/SourcePart.js')(mongoose),
 	Country: require('../models/mongoose/Country.js')(mongoose),
 	CategoryParts: require('../models/mongoose/CategoryParts.js')(mongoose),
 	Parts: require('../models/mongoose/Parts.js')(mongoose)

}