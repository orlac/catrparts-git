module.exports = function (grunt) {
	grunt.registerTask('linkAssets', [
		//'sails-linker:devJs',
		'sails-linker:adminDevJs',
		'sails-linker:indexDevJs',
		//'sails-linker:devStyles',
		'sails-linker:indexDevStyles',
		'sails-linker:adminDevStyles',
		'sails-linker:devTpl',
		'sails-linker:devJsJade',
		'sails-linker:devStylesJade',
		'sails-linker:devTplJade'
	]);
};
