/**

*/
module.exports = function(grunt) {

	// grunt.registerMultiTask('closureDeps', 'closure deps',  function(){
	// 	throw new Error();		
	// });
	//throw new Error(); 
	grunt.config.set('closureDepsWrite', {
			dev:{

				options: {
					depswriter: 'tasks/closure/bin/build/depswriter.py',
				    root: 'assets',
				    output_file: 'assets/closure/goog/deps.js',
				    root_with_prefix: 'assets/app ../../app',
			  	},
				
			}
	});

	grunt.loadNpmTasks('grunt-closure-deps-write');
};