module.exports = function(grunt) {

	grunt.config.set('browserify', {
			index:{
				options: {
    				browserifyOptions: {
				        debug: true
			      	}	
    			},
				src: 'assets/app/index.js',
    			dest: 'assets/js/index/index.js',
			},
			admin:{
				options: {
    				browserifyOptions: {
				        debug: true
			      	}	
    			},
				src: 'assets/app/admin.js',
    			dest: 'assets/js/admin/admin.js',
			}
	});

	grunt.loadNpmTasks('grunt-browserify');
};