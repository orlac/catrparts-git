module.exports.admin = [
  
  // Load sails.io before everything else
  'js/dependencies/sails.io.js',

  // Dependencies like jQuery, or Angular are brought in here
  'js/dependencies/**/*.js',

  // All of the rest of your client-side js files
  // will be injected here in no particular order.
  'js/admin/**/*.js'
];

module.exports.index = [
  
  // Load sails.io before everything else
  'js/dependencies/sails.io.js',

  // Dependencies like jQuery, or Angular are brought in here
  'js/dependencies/**/*.js',

  // All of the rest of your client-side js files
  // will be injected here in no particular order.
  'js/index/**/*.js'
];


module.exports.adminCss = [
  
  //'styles/admin/**/*.css',
  'styles/admin/*.css'
];

module.exports.indexCss = [
  
  //'styles/index/**/*.css',
  'styles/index/*.css'
];