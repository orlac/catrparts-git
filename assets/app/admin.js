var cssify = require('cssify');
var $ = require('jquery');
window.jQuery = $;
window.$ = $;
var bs = require('bootstrap');
require('angular');
require('ui-bootstrap-tpls');

var formRow = require('./common/directives/formRow/dir.js');
var adminControl = require('./common/directives/adminControl.js');
var gridPager = require('./common/directives/pager/dir.js');
var templateFactory = require('./common/services/templateFactory.js');

require('angular-ui-router');
require('angular-animate');
require('angular-loading-bar');
cssify.byUrl('/bower_components/angular-loading-bar/build/loading-bar.min.css');

//var auth = require('./auth/mod.js');

var init = require('./common/initApp.js');

var apps = [
	//require('./auth/mod.js').init().name	
	,require('./admin/navigate/mod.js').init().name	
	,require('./admin/categoryParts/mod.js').init().name	
	,require('./admin/sourcePart/mod.js').init().name	
	,require('./admin/parts/mod.js').init().name	
];

init.app.requires = init.app.requires.concat(apps);

//todo в отдельный файл общие директивы и тд.
init.app.factory('templateFactory', [templateFactory]);
init.app.directive('formRow', [formRow]);
init.app.directive('adminControl', [
    '$injector'
    ,adminControl
]);
init.app.requires=init.app.requires.concat( adminControl.requires || [] );
init.app.directive('gridPager', ['templateFactory', gridPager]);



init.start();
//var app = angular.module('myApp', ['ui.tree']);
//console.log(application);