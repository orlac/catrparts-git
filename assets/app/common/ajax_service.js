/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 var $ = require('jquery');
 require('notify-custom');


var _fn=function($http){

    var service={};    

    var errfn=function(data, status, headers, config){
        data = data || {};
        data.error=data.error || "какая-то непонятная ошибка ! (error "+status+")";
        $.notify(data.error, "error");
    };

    service.send=function(config, _success, _error){
        config.responseType = config.responseType ||  'json';

        $http(config).
            success(function(data, status, headers, config) {
                if(_success){
                    if(config.responseType === 'json'){
                        if(data.success){
                            _success(data, status, headers, config);
                        }
                        else if(data.error){
                            if(_error){
                                _error(data, status, headers, config);
                            }else{
                                errfn(data, status, headers, config);
                            }
                        }else{
                            errfn(data, status, headers, config);
                        }
                    }else{
                        //_success(data, status, headers, config);
                    }
                }
            }).
            error(function(data, status, headers, config) {
                if(config.responseType === 'json'){
                    errfn(data, status, headers, config);
                }else{
                    errfn({error: data}, status, headers, config);
                }
            });
    };
    return service;
};

module.exports = _fn;
    
    



