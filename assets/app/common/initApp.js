/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var service = require('./ajax_service.js');
var ng = angular.module('application', []);
        
ng.factory('app.common.ajax_service', ['$http', function($http){
    return new service($http);
}] );

var _start = function(){
    angular.element(document).ready(function() {
        angular.bootstrap(document, [ng.name]);
    });    
}


module.exports = {
    app: ng,
    start: _start
};

// module.exports = function(){
    
    
    
//     var ready = false;
//     var _fn=function(){
        

//         ready=true;
//         return ng;
//     };
    
//     var _i=function(cb){
//         if(!ready){
//             _fn();
//         }
//         cb(angular.module('application'));
//     };
    
//     return _i;
// };