'use strict';

module.exports =  function($config, $dataService, sourceMethod){
        
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.columns = $scope.columns || $config.columns || [];

        $scope.edit = function(model){
            $scope.goEdit(model);
        };

        $scope.delete = function(model){
            if(confirm('Удалить запись ?')){
                $scope.goDelete(model, function(){
                    $scope.refresh();
                });
            }
        };
        
    };
    
    return {
        restrict: 'E',
        scope: {
            columns: '=',
            goDelete: '=',
            goEdit: '=',
            refresh: '='
        },
        controller: _ctrl,
        templateUrl: '/app/common/directives/modelSelect/tmpl.html',
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 