'use strict';

module.exports = function($injector){
        
    var _ctrl = function($scope, $element, $attrs, $injector){ 
        
        $scope.edit = function(){
            $scope.onEdit($scope.model);
        };

        $scope.delete = function(){
            $scope.onDelete($scope.model);
        };
    };
    
    return {
        restrict: 'E',
        scope: {
            model: '='
            ,onEdit: '='
            ,onDelete: '='
        },
        controller: _ctrl,
        templateUrl: '/app/common/directives/adminControl.html',
        link: function(scope, elm, attrs, ctrl) {
            
        }
    };
}; 
//_fn.requires=['ui.bootstrap'];