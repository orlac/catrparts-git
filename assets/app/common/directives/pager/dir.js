'use strict';

module.exports = function(templateFactory){
        
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        var viewPagesCount = 5;

        $scope.getCanPrev=function(){
          return $scope.pagerData.curPage > 0;
        };
        $scope.getCanNext=function(){
          return $scope.pagerData.countPages-1 > $scope.pagerData.curPage;
        };

        $scope.getCanFirst=function(){
          return ($scope.pagerData.curPage - viewPagesCount) > 0;
        };

        $scope.getCanLast=function(){
          return ($scope.pagerData.curPage - viewPagesCount) < $scope.pagerData.countPages;
        };


        $scope.getIsActive=function(page){
          return $scope.pagerData.curPage == page;
        };

        $scope.prev=function(){
          $scope.gotoPage( Math.max(0, $scope.pagerData.curPage - 1) );
        };

        $scope.next = function(){
          $scope.gotoPage( Math.min($scope.pagerData.countPages-1, $scope.pagerData.curPage + 1) );  
        };

        $scope.getPages = function(){

          var start = Math.max(0, $scope.pagerData.curPage - viewPagesCount);
          var end = Math.min($scope.pagerData.countPages-1, start + (viewPagesCount*2) );

          var pages = [];
          for(start; start <= end; start++){
            pages.push(start)
          }
          return pages;
        };
    };
    
    return {
        restrict: 'E',
        scope: {
          pagerData: '=',
          gotoPage: '='
        },
        controller: _ctrl,
        templateUrl: templateFactory.get('/app/common/directives/pager/tmpl.html'),
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 
