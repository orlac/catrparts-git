'use strict';

angular.module('dropZone', []).directive('dropZone', function() {
    return {
      restrict: 'A',
      scope: {
        onFile: '=',
        hoverClass: '=',
      },
      link: function(scope, element, attrs) {

      	var nel = element[0];
      	nel.ondragover = function () { 
      		element.addClass(scope.hoverClass);
      		return false; 
      	};
		nel.ondragend = function () { 
			element.removeClass(scope.hoverClass);
      		return false; 
			
		};
		nel.ondrop = function (e) {
			element.removeClass(scope.hoverClass);
			e.preventDefault();
			scope.onFile(e.dataTransfer.files);
		}
      }
    };
});