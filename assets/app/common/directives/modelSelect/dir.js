'use strict';

module.exports =  function($dataService, sourceMethod){
        
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.list = [];

        $dataService[sourceMethod](function(data){
            $scope.list = data;
            $scope.$apply();
        });
        
    };
    
    return {
        restrict: 'E',
        scope: {
            model: '='
        },
        controller: _ctrl,
        templateUrl: '/app/common/directives/modelSelect/tmpl.html',
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 