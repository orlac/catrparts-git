'use strict';

module.exports =  function(){
        
    var _ctrl = function($scope, $element, $attrs, $injector){};
    
    return {
        restrict: 'E',
        scope: {
            model: '=',
            label: '='
        },
        controller: _ctrl,
        templateUrl: '/app/common/directives/formRow/tmpl.html',
        link: function(scope, elm, attrs, ctrl) {

        }
    };
};