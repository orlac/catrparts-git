'use strict';

module.exports =  function($dataService){
        
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.list = [];

        $dataService.getCountries(function(data){
            $scope.list = data;
            $scope.$apply();
        });
        
    };
    
    return {
        restrict: 'E',
        scope: {
            model: '='
        },
        controller: _ctrl,
        templateUrl: '/app/common/directives/countrySelect/tmpl.html',
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 
    