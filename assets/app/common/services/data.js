'use strict';

module.exports = function($http, $injector){
    console.log('app.common.services.data');    
    
    var s = {};
    

    s.getCountries=function(onGetItems){
        $injector.get('app.common.ajax_service').send({
            url: '/country/listData',
            method: 'get',
        }, function(data){
            onGetItems(data.data);    
        });
    };  

    s.getSources=function(onGetItems){
        $injector.get('app.common.ajax_service').send({
            url: '/data/sourceParts',
            method: 'get',
        }, function(data){
            onGetItems(data.data);    
        });
    };      
            
    return s;
}; 