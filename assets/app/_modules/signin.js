/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function(){
    
    //$script.path('/app/');
    $script([
            '/app/auth/controllers/SignIn.js'
            ,'/app/auth/services/auth.js'
            ,'/app/lib/ajax_service.js'
            ,'/app/plugins/notify-custom.js'
        ]
        ,'signin');
        
    $script.ready('signin', function(){
        
        var _requires=[];
        var mod = angular.module('form.signin', []);
        
        /** service */
        mod.factory('app.auth.services.auth', ['$http', '$injector',   function($http, $i){
                return new app.auth.services.auth($http, $i);
        }] );
        _requires=_requires.concat( app.auth.services.auth.requires || [] );
        
        /** controllers */
        /** controllers */
    
        mod.controller('app.auth.controllers.SignIn', [
            '$scope'
            ,'$element'
            ,'$attrs'
            ,'$injector'
            ,'app.auth.services.auth'
            ,app.auth.controllers.SignIn ]);
        _requires=_requires.concat( app.auth.controllers.SignIn.requires || [] );
        
        _requires = $.unique(_requires);
        for(var i in _requires){
            mod.requires.push( _requires[i] );
        }
        
        
        //todo require in main app
    });
    
    
})();




//goog.app_require('app.auth.controllers.SignIn');



