/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('ui-bootstrap-tpls');

var _fn = function( $comService, $window, navigateProvider, templateFactory){
    
    var _ctrl = function($scope, $element, $attrs){
        $scope.items=[];

        $comService.getItems(function(items){

            $scope.items = items;
            for(var i in items){
                if(items[i].state){
                    navigateProvider.addState(
                        items[i].state, 
                            {
                                url: items[i].url,
                                abstract: true,
                                template: '<div ui-view></div>' 
                            });    
                }
            }
            $scope.$apply();
            var _hash = $window.location.hash;
            if(_hash){
                $window.location.hash = '/';
                setTimeout(function(){
                    $window.location.hash = _hash.replace('#', '');    
                }, 0);
            }
        });
    };
    
    return {
        restrict: 'E',
        scope: {},
        controller: _ctrl,
        templateUrl: function(elem, attr){
          return templateFactory.get( attr.templateUrl || '/app/index/navigate/navigate.html' );
        },
        link: function(scope, elm, attrs, ctrl) {
            
        }
    };
}; 
_fn.requires=['ui.bootstrap'];

module.exports =_fn;