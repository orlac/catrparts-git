/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';
var _fn = function($http, $injector){
    
    var s = {};
    s.getItems=function(onGetItems){

        $injector.get('app.common.ajax_service').send({
            url: '/index/menu',
            method: 'get',
        }, function(data){
            onGetItems(data.data);    
        });

        // var items = [
        //     {
        //         label: 'Главная'
        //         ,url: '/'
        //         ,state: 'home'
        //         //,ico: 'fa fa-truck '
        //     }
        //     ,{
        //         label: 'О компании'
        //         ,url: '/about'
        //         ,state: 'about'
        //         //,ico: 'fa fa-cogs'
        //     }
        //     ,{
        //         label: 'Помощь'
        //         ,url: '/help'
        //         ,state: 'help'
        //         //,ico: 'fa fa-barcode'
        //     }
        //     ,{
        //         label: 'Каталог'
        //         ,url: '/parts'
        //         ,state: 'parts'
        //         //,ico: 'fa fa-barcode'
        //     }
        //     ,{
        //         label: 'Войти'
        //         ,url: '/signin'
        //         ,state: 'signin'
        //         //,ico: 'fa fa-thumbs-up'
        //     }
        //     ,{
        //         label: 'Регистрация'
        //         ,url: '/signup'
        //         ,state: 'signup'
        //         //,ico: 'fa fa-thumbs-up'
        //     }
        //     //TODO if login|no login
        // ];
        // onGetItems(items);
    };
            
    return s;
}; 

module.exports =_fn;
