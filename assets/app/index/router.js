(function(){
    'use strict';

    module.exports = function($stateProvider, $urlRouterProvider) {

        

        $urlRouterProvider.when('', '/index');

        //главная
        $stateProvider.state( 'home.index', {
            url: 'index',        
            // templateUrl: function(templateFactory){
            //     return templateFactory.get('/app/index/templates/index.html');
            // },
            templateUrl: '/app/index/templates/index.html',
            controller:  function($scope, $stateParams, $rootScope, rndBg){
                $scope.classBg = rndBg.getBg();
                $rootScope.pageTitle="Главная"
            }
        });

        //about
        $stateProvider.state( 'about.index', {
            url: '',        
            templateUrl: '/app/index/templates/about.html',
            controller:  function($scope, $stateParams, $rootScope, rndBg){
                $scope.classBg = rndBg.getBg();
                $rootScope.pageTitle="О компании"
            }
        });

        //войти
        $stateProvider.state( 'signin.index', {
            url: '',        
            templateUrl: '/app/index/templates/signin.html',
            controller:  function($scope, $stateParams, $rootScope, rndBg){
                $scope.classBg = rndBg.getBg();
                $rootScope.pageTitle="Вход для клиентов"
            }
        });

        //регистрация
        $stateProvider.state( 'signup.index', {
            url: '',        
            templateUrl: '/app/index/templates/signup.html',
            controller:  function($scope, $stateParams, $rootScope, rndBg){
                $scope.classBg = rndBg.getBg();
                $rootScope.pageTitle="Регистрация"
            }
        });

    };    
})();
