(function(){
	'use strict';
	module.exports = function($stateProvider, $urlRouterProvider, rootState) {

		var actions = [];
	    var header = 'Категории';

	    //$urlRouterProvider.otherwise("/list");
	    $urlRouterProvider.when('/'+rootState, '/'+rootState+'/list');

	    $stateProvider.state( rootState+'.index', {
	        url: '/list',        
	        templateUrl: '/app/admin/categoryParts/templates/index.html', 
	        controller: function($scope) {
	            $scope.$parent.actions = actions;
	            $scope.$parent.header = header;
	        }
	    });
	}	
})();
