'use strict';

module.exports = function($http, $injector){
    console.log('app.admin.categoryParts.services.common');    
    
    var s = {};
    
    s.getTree = function(cb){
    	$injector.get('app.common.ajax_service').send({
            url: '/admin/categoryParts/tree',
            method: 'get'
        }, function(data){
            cb(data.tree);    
        });
    }

    s.save = function(data, cb){
    	console.log('save data', data);
        $injector.get('app.common.ajax_service').send({
            url: '/admin/categoryParts/save',
            method: 'post',
            data: data
        }, function(data){
            cb(data.model);    
        });
    },

    s.remove = function(_id, cb){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/categoryParts/remove',
            method: 'post',
            data: {
            	_id: _id
            }
        }, function(data){
            cb(data.model);    
        });
    }
            
    return s;
}; 
