'use strict';
require('angular-ui-tree');

var cssify = require('cssify');


var _fn = function($comService){
    
    var tmpl = '/app/admin/categoryParts/directives/tree.html';
    var templateTreItemUrl = '/app/admin/categoryParts/directives/treeItem.html';

    var _ctrl = function($scope, $element, $attrs){
        
        $scope.templateTreItemUrl = $attrs.templateTreItemUrl || templateTreItemUrl;

        $scope.tree=[];

        

        $scope.appendNew = function(){
            $scope.tree.unshift({
                _id: null,
                title: "",
                children: []
            });
        };     

        $scope.newSubItem = function(scope) {
          var nodeData = scope.$modelValue;
          nodeData.children = nodeData.children || [];
          nodeData.children.unshift({
            _id: null,
            title: '',
            //title: nodeData.title + '.' + (nodeData.nodes.length + 1),
            nodes: []
          });
        };

        $scope.remove = function(scope) {
          scope.remove();
        };

        $scope.toggle = function(scope) {
          scope.toggle();
        };


        $scope.save = function(){
            $comService.save( { tree: $scope.tree }, function(){
                _refresh();
            } );
        };

        $scope.delete = function(model){
            model.isDeleted = true;
            // $comService.remove( { tree: $scope.tree }, function(){
            //     _refresh();
            // } );
        };

        var _refresh = function(){
            $comService.getTree(function(tree){
                $scope.tree=tree;                
                $scope.$apply();
            });
        }
        _refresh();
        
    };
    
    return {
        restrict: 'E',
        scope: {
            
        },
        controller: _ctrl,
        templateUrl: function(elem, attr){
            return attr.templateUrl || tmpl;
        },
        link: function(scope, elm, attrs, ctrl) {
            cssify.byUrl('/app/admin/categoryParts/directives/treeCss.css');
            cssify.byUrl('/bower_components/angular-ui-tree/dist/angular-ui-tree.min.css');
        }
    };
}; 
_fn.requires=['ui.tree'];


module.exports = _fn;