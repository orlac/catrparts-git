'use strict';
require('angular-ui-router');
var serv = require('./services/common.js');
var dir = require('./directives/tree.js');
var router = require('./router.js');


var init = function(){
    
    var mod = angular.module('admin.categoryParts', ['ui.router']);


    /** service */
    mod.factory('app.admin.categoryParts.services.common', ['$http', '$injector',   function($http, $i){
            return new serv($http, $i);
    }] );
    mod.requires=mod.requires.concat( serv.requires || [] );

    
    /** directives */
    
    mod.directive('categoryPartsTree', [
        'app.admin.categoryParts.services.common'
        ,dir
    ]);
    mod.requires=mod.requires.concat( dir.requires || [] );


    /** routing */

    mod.config( ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
        return new router($stateProvider, $urlRouterProvider, 'categoryParts');
    } ] );      

    return mod;  
};

module.exports = {
    init: init
};