'use strict';
require('angular-ui-router');
var service = require('./services/common.js');
var dirList = require('./directives/list.js');
var dirEdit = require('./directives/edit.js');
var router = require('./router.js');

//common
var serviceData = require('../../common/services/data.js');
var dirCountry = require('../../common/directives/countrySelect/dir.js');
var adminControl = require('../../common/directives/adminControl.js');


var init = function(){
    
    var mod = angular.module('admin.sourcePart', ['ui.router']);
    var _requires=[];
    

    /**
    * common
    */
    mod.factory('app.common.services.data', ['$http', '$injector',   function($http, $i){
            return new serviceData($http, $i);
    }] );
    //country
    mod.directive('countrySelect', [
        'app.common.services.data'
        ,dirCountry
    ]);


    /** service */
    mod.factory('app.admin.sourcePart.services.common', ['$http', '$state', '$injector',   function($http, $state, $i){
            return new service($http, $state, $i);
    }] );
    mod.requires=mod.requires.concat( service.requires || [] );

    
    /** directives */
    
    mod.directive('sourcePartList', [
        '$injector'
        ,'app.admin.sourcePart.services.common'
        ,'$state'
        ,dirList
    ]);
    mod.requires=mod.requires.concat( dirList.requires || [] );

    mod.directive('sourcePartEdit', [
        '$injector'
        ,'app.admin.sourcePart.services.common'
        ,dirEdit
    ]);
    mod.requires=mod.requires.concat( dirEdit.requires || [] );

    // mod.directive('adminControl', [
    //     '$injector'
    //     ,adminControl
    // ]);
    // mod.requires=mod.requires.concat( adminControl.requires || [] );

    /** routing */

    mod.config( ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
        return new router($stateProvider, $urlRouterProvider, 'sourcePart');
    } ] ); 

    // mod.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
    //     $rootScope.$on('onSave', function(event, data) { 
    //         $state.go('list');    
            
    //     });
    // }]);

    return mod;
    
};

module.exports = {
    init: init
};