(function(){
    'use strict';

    var _view = 'view0'
    

    module.exports = function($stateProvider, $urlRouterProvider, rootState) {

        var actions = [
            { label: 'Создать', sref: rootState+'.add' },
            { label: 'Список', sref: rootState+'.index' }
        ]
        var header = 'Производители';

        //$urlRouterProvider.otherwise('/'+rootState+'/list');
        $urlRouterProvider.when('/'+rootState, '/'+rootState+'/list');

        $stateProvider.state( rootState+'.index', {
            url: '/list',        
            //templateUrl: '/admin/sourcePart/list'
            template: '<source-part-list></source-part-list>',
            controller: function($scope) {
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
            }
        });
        $stateProvider.state(rootState+'.edit', {
            url: '/edit/:id',        
            template: '<source-part-edit></source-part-edit>',
            controller: function($scope, $stateParams){
                $scope.mid = $stateParams.id;
                $scope.$parent.mid = $stateParams.id;
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
                $scope.$parent.actionHeader = 'Редактирование';
            } 
            
        });
        $stateProvider.state(rootState+'.add', {
            url: '/add',
            //templateUrl: '/admin/sourcePart/add'
            template: '<source-part-edit></source-part-edit>',
            controller: function($scope) {
                $scope.mid = null;
                $scope.$parent.mid = null;
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
                $scope.$parent.actionHeader = null;
            }
        });

    };    
})();
