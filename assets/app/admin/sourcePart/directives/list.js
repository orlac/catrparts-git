'use strict';

module.exports = function($injector, $comService, $state){
        
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.items=[];
        
        

        $scope.edit = function(model){
            $comService.goToEdit(model.mid);
            //$state.go('sourcePart.edit', {id: model.mid});
        };

        $scope.delete = function(model){
            if(confirm('Удалить запись ?')){
                $comService.deleteItem(model._id, function(){
                    _refresh();
                });    
            }
        };
        
        var _refresh = function(){
            $comService.getItems(function(items){
                $scope.items = items;
                $scope.$apply();
            });    
        };

        _refresh();
    };
    
    return {
        restrict: 'E',
        scope: {},
        controller: _ctrl,
        templateUrl: '/app/admin/sourcePart/directives/list.html',
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 
