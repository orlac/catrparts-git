'use strict';

module.exports = function($http, $state, $injector){
    console.log('app.admin.sourcePart.services.common');    
    
    var s = {};

    s.goToList=function(){
        $state.go('sourcePart.index');
    };

    s.goToEdit=function(id){
        $state.go('sourcePart.edit', {id: id});
        //$state.transitionTo('/sourcePart/edit/42');
    };

    s.getItems=function(onGetItems){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/sourcePart/listData',
            method: 'get',
        }, function(data){
            onGetItems(data.data);    
        });
    };

    s.getItem = function(mid, onGetItem){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/sourcePart/get',
            params: {
                mid: mid
            },
            method: 'get',
        }, function(data){
            onGetItem(data.data);    
        });
    };

    s.deleteItem = function(_id, cb){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/sourcePart/delete',
            params: {
                _id: _id
            },
            method: 'get',
        }, function(data){
            cb(data.data);    
        });
    };

    s.save = function(data, onSave){
        console.log('save data', data);
        $injector.get('app.common.ajax_service').send({
            url: '/admin/sourcePart/save',
            method: 'post',
            data: data
        }, function(data){
            onSave(data.model);    
        });
    }
            
    return s;
}; 
