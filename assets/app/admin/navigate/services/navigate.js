/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';
var _fn = function($http, $injector){
    console.log('app.admin.navigate.services.navigate');    
    
    var s = {};
    s.getItems=function(onGetItems){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/index/menu',
            method: 'get',
        }, function(data){
            onGetItems(data.data);    
        });
    };
            
    return s;
}; 

module.exports =_fn;
