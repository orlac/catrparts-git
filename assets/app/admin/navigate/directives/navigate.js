/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require('ui-bootstrap-tpls');

var _fn = function( $comService, $window, navigateProvider){
    
    var _ctrl = function($scope, $element, $attrs){
        
        $scope.items=[];
        
        $comService.getItems(function(items){

            $scope.items = items;
            for(var i in items){
                navigateProvider.addState(
                    items[i].state, 
                        {
                            url: items[i].url,
                            abstract: true, 
                            //template: '<div ui-view></div>'
                            templateUrl: '/app/admin/template.html'
                            //template: '<div>vberhjbvherbierbiernbuiernbui</div>'
                        });
            }
            $scope.$apply();
            var _hash = $window.location.hash;
            if(_hash){
                $window.location.hash = '/';
                setTimeout(function(){
                    $window.location.hash = _hash.replace('#', '');    
                }, 0);
            }
            //$window.location.hash = _hash.replace('#', '')+'?'+Math.random();
        });
        
    };
    
    return {
        restrict: 'E',
        scope: {},
        controller: _ctrl,
        templateUrl: '/app/admin/navigate/directives/navigate.html',
        link: function(scope, elm, attrs, ctrl) {
            
            $($window).resize(function() {
                if($(window).width() >= 765){
                    $(".sidebar .sidebar-inner", elm).slideDown(350);
                }
                else{
                    $(".sidebar .sidebar-inner", elm).slideUp(350); 
                }
            });

            $(".sidebar-dropdown a", elm).on('click',function(e){
                  e.preventDefault();

                if(!$(this).hasClass("dropy")) {
                    // hide any open menus and remove all other classes
                    $(".sidebar .sidebar-inner", elm).slideUp(350);
                    $(".sidebar-dropdown a", elm).removeClass("dropy");
                    
                    // open our new menu and add the dropy class
                    $(".sidebar .sidebar-inner", elm).slideDown(350);
                    $(this).addClass("dropy");
                }
                  
                else if($(this).hasClass("dropy")) {
                    $(this).removeClass("dropy");
                    $(".sidebar .sidebar-inner", elm).slideUp(350);
                }
            });

        }
    };
}; 
_fn.requires=['ui.bootstrap'];

module.exports =_fn;