/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict'; 
require('angular-ui-router');
var cssify = require('cssify');
cssify.byUrl('/app/admin/navigate/style/style.css');
var serv = require('./services/navigate.js');
var dir = require('./directives/navigate.js');

var init = function(){
    
    var mod = angular.module('admin.navigate', ['ui.router']);
    
    /** service */
    mod.factory('navigateService', ['$http', '$injector',  function($http, $i){
            return new serv($http, $i);
    }] );
    mod.requires=mod.requires.concat( serv.requires || [] );
    

    mod.provider('navigateProvider', ['$stateProvider',  function($stateProvider){
            this.$get = function($q, $timeout, $state) { // for example
                return { 
                  addState: function(name, state) { 
                    $stateProvider.state(name, state);
                  },
                  state: function(){
                    return $state;
                  }
                }
              }
    }] );

    /** directives */
    
    mod.directive('adminNavigate', [
        'navigateService'
        ,'$window'
        ,'navigateProvider'
        ,dir
    ]);
    mod.requires=mod.requires.concat( dir.requires || [] );

    return mod;
    
};

module.exports = {
    init: init
};