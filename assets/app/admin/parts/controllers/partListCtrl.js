/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var _fn = function($scope, $comService){
    

    $scope.artistsColumnDef = [
	    {
	      columnHeaderDisplayName: '#',
	      displayProperty: 'mid'
	    },{
	      columnHeaderDisplayName: 'Товар',
	      displayProperty: 'myNumPart'
	    },{
	      columnHeaderDisplayName: 'Наименование',
	      displayProperty: 'name'
	    },{
	      columnHeaderDisplayName: 'Розница',
	      displayProperty: 'price'
	    },{
	      columnHeaderDisplayName: 'Закупка',
	      displayProperty: 'min'
	    },{
	      columnHeaderDisplayName: 'Ориг.№',
	      displayProperty: 'min'
	    },{
	      columnHeaderDisplayName: 'Производитель',
	      displayProperty: 'min'
	    },{
	      columnHeaderDisplayName: 'ДопПоле8',
	      displayProperty: 'min'
	    },{
	      columnHeaderDisplayName: 'Ориг.товар',
	      displayProperty: 'min'
	    }
	    
	];

	$scope.listConfig = $comService.getListConfig();

	$scope.edit = function(model){
        $comService.goToEdit(model.mid);
    };

    $scope.delete = function(model){
        if(confirm('Удалить запись ?')){
            $comService.deleteItem(model._id, function(){
                _refresh();
            });    
        }
    };

}; 
    
module.exports = _fn;