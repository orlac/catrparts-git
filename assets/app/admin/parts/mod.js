'use strict';
require('angular-ui-router');
var service = require('./services/common.js');
var dirList = require('./directives/list.js');
var dirEdit = require('./directives/edit.js');
var partsImport = require('./directives/import.js');
var router = require('./router.js');

//common
var serviceData = require('../../common/services/data.js');
var dirParts = require('../../common/directives/modelSelect/dir.js');
var adminControl = require('../../common/directives/adminControl.js');


var init = function(){
    
    var mod = angular.module('admin.parts', ['ui.router']);
    var _requires=[];
    

    /**
    * common
    */
    mod.factory('app.common.services.data', ['$http', '$injector',   function($http, $i){
            return new serviceData($http, $i);
    }] );
    //country
    mod.directive('sourceSelect', [
        'app.common.services.data', function(serv){
            return new dirParts(serv, 'getSources');
        }
    ]);


    /** service */
    mod.factory('app.admin.parts.services.common', ['$http', '$state', '$injector',   function($http, $state, $i){
            return new service($http, $state, $i);
    }] );
    mod.requires=mod.requires.concat( service.requires || [] );

    
    /** directives */
    
    mod.directive('partsList', [
        '$injector'
        ,'app.admin.parts.services.common'
        ,'$state'
        ,'templateFactory'
        ,dirList
    ]);
    mod.requires=mod.requires.concat( dirList.requires || [] );

    mod.directive('partsEdit', [
        '$injector'
        ,'app.admin.parts.services.common'
        ,dirEdit
    ]);
    mod.requires=mod.requires.concat( dirEdit.requires || [] );

    mod.directive('partsImport', [
        'app.admin.parts.services.common'
        ,'templateFactory'
        ,partsImport
    ]);
    mod.requires=mod.requires.concat( partsImport.requires || [] );

    // mod.directive('adminControl', [
    //     '$injector'
    //     ,adminControl
    // ]);
    // mod.requires=mod.requires.concat( adminControl.requires || [] );

    /** routing */

    mod.config( ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
        return new router($stateProvider, $urlRouterProvider, 'parts');
    } ] ); 
    return mod;
    
};

module.exports = {
    init: init
};