(function(){
    'use strict';

    module.exports = function($stateProvider, $urlRouterProvider, rootState) {

        var actions = [
            { label: 'Создать', sref: rootState+'.add' },
            { label: 'Список', sref: rootState+'.index' },
            { label: 'Импорт', sref: rootState+'.import' }
        ]
        var header = 'Товары';

        //$urlRouterProvider.otherwise('/'+rootState+'/list');
        $urlRouterProvider.when('/'+rootState, '/'+rootState+'/list');

        //список
        $stateProvider.state( rootState+'.index', {
            url: '/list',        
            //templateUrl: '/admin/sourcePart/list'
            template: '<parts-list></parts-list>',
            controller: function($scope) {
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
                $scope.$parent.actionHeader = null;
            }
        });

        //exel импорт
        $stateProvider.state(rootState+'.import', {
            url: '/import',
            //templateUrl: '/admin/sourcePart/add'
            template: '<parts-import></parts-import>',
            controller: function($scope) {
                $scope.mid = null;
                $scope.$parent.mid = null;
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
                $scope.$parent.actionHeader = 'Импорт';
            }
        });

        //добаввить
        $stateProvider.state(rootState+'.edit', {
            url: '/edit/:id',        
            template: '<parts-edit></parts-edit>',
            controller: function($scope, $stateParams){
                $scope.mid = $stateParams.id;
                $scope.$parent.mid = $stateParams.id;
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
                $scope.$parent.actionHeader = 'Редактирование';
            } 
        });

        //добаввить
        $stateProvider.state(rootState+'.add', {
            url: '/add',        
            template: '<parts-edit></parts-edit>',
            controller: function($scope, $stateParams){
                $scope.mid = null;
                $scope.$parent.mid = $stateParams.id;
                $scope.$parent.actions = actions;
                $scope.$parent.header = header;
                $scope.$parent.actionHeader = 'Добавление';
            } 
        });        

    };    
})();
