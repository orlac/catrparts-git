'use strict';

require('../../../common/directives/dropZone.js');
var cssify = require('cssify');


var _fn = function($comService, templateFactory){
    
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.files=[];
        $scope.percent=0;

        $scope.onFile = function(files){
            $scope.files = files;
            $scope.$apply();
        };

        var _progress = false;
        $scope.isProgress = function(files){
            return _progress;
        };

        $scope.upload = function(){
            $scope.percent=0;
            _progress=true;
            $comService.import($scope.files, {
                uploadProgress: uploadProgress,
                uploadComplete: uploadComplete,
                uploadFailed: uploadFailed,
                uploadCanceled: uploadCanceled
            });
        }

        function uploadProgress(evt) {
            $scope.$apply(function(){
                if (evt.lengthComputable) {
                    $scope.percent = Math.round(evt.loaded * 100 / evt.total)
                } else {
                    $scope.percent = 0;
                }
            });
        };

        function uploadComplete(evt) {
            /* This event is raised when the server send back a response */
            $scope.$apply(function(){
                _progress = false;
            });
            alert(evt.target.responseText)
        }

        function uploadFailed(evt) {
            $scope.$apply(function(){
                _progress = false;
            });
            alert("There was an error attempting to upload the file.");
        }

        function uploadCanceled(evt) {
            $scope.$apply(function(){
                _progress = false;
            });
        }
    };
    
    return {
        restrict: 'E',
        scope: {
            onSave: '='
        },
        controller: _ctrl,
        templateUrl: templateFactory.get('/app/admin/parts/directives/import.html'),
        link: function(scope, elm, attrs, ctrl) {
            cssify.byUrl('/app/admin/parts/directives/import.css');
        }
    };
}; 
_fn.requires=['dropZone'];

module.exports = _fn;