'use strict';

module.exports =  function($injector, $comService){
    
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.mid = $scope.mid || $scope.$parent.mid
        $scope.item = {};
        $scope.countryList = {};

        if( angular.isDefined($scope.mid) ){
            $comService.getItem( $scope.mid,  function(model){
                $scope.item = model;
                $scope.$apply();
            });   
        }

        $scope.submit = function(){

            var data = angular.copy($scope.item);

            $comService.save( data,  function(model){
                $comService.goToList(model.mid);
            });                
        }
        
    };
    
    return {
        restrict: 'E',
        scope: {
            onSave: '='
            //редактрование
            //,mid: '='
        },
        controller: _ctrl,
        templateUrl: '/app/admin/parts/directives/edit.html',
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 
