'use strict';

module.exports = function($injector, $comService, $state, templateFactory){
        
    var _ctrl = function($scope, $element, $attrs, $injector){
        
        $scope.items=[];
        $scope.pager={};
        
        $scope.columns = [
            {
              header: '#',
              value: 'mid'
            },{
              header: 'Товар',
              value: 'numPart'
            },{
              header: 'Наименование',
              value: 'name'
            },{
              header: 'Розница',
              value: 'price'
            },{
              header: 'Закупка',
              value: 'myPrice'
            },{
              header: 'Ориг.№',
              value: 'myNumPart'
            },{
              header: 'Производитель',
              value: function(val){
                return (val.source)? val.source.name : '';
              }
            },{
              header: 'ДопПоле8',
              value: 'extParam'
            },{
              header: 'Ориг.товар',
              value: 'numPart2'
            }
        ];
        
        $scope.getViewValue=function(item, value){
            if(typeof value === 'function'){
                return value(item);
            }else{
                return item[value];
            }
        };

        $scope.edit = function(model){
            $comService.goToEdit(model.mid);
            //$state.go('sourcePart.edit', {id: model.mid});
        };

        $scope.delete = function(model){
            if(confirm('Удалить запись ?')){
                $comService.deleteItem(model._id, function(){
                    _refresh();
                });    
            }
        };
        
        
        var _refresh = function(page){
            $comService.getItems({page: page}, function(items, pager){
                $scope.items = items;
                $scope.pager = pager;
                $scope.$apply();
            });    
        };
        $scope.gotoPage = _refresh;

        _refresh();
    };
    
    return {
        restrict: 'E',
        scope: {},
        controller: _ctrl,
        templateUrl: templateFactory.get('/app/admin/parts/directives/list.html'),
        link: function(scope, elm, attrs, ctrl) {

        }
    };
}; 
