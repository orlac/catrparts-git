'use strict';

module.exports = function($http, $state, $injector){
    console.log('app.admin.parts.services.common');    
    
    var s = {};

    s.goToList=function(){
        $state.go('parts.index');
    };

    s.goToEdit=function(id){
        $state.go('parts.edit', {id: id});
        //$state.transitionTo('/parts/edit/42');
    };

    s.getItems=function(data, onGetItems){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/parts/listData',
            method: 'get',
            params: data
        }, function(data){
            onGetItems(data.data, data.pager);    
        });
    };

    s.getItem = function(mid, onGetItem){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/parts/get',
            params: {
                mid: mid
            },
            method: 'get',
        }, function(data){
            onGetItem(data.data);    
        });
    };

    s.deleteItem = function(_id, cb){
        $injector.get('app.common.ajax_service').send({
            url: '/admin/parts/delete',
            params: {
                _id: _id
            },
            method: 'get',
        }, function(data){
            cb(data.data);    
        });
    };

    s.save = function(data, onSave){
        console.log('save data', data);
        $injector.get('app.common.ajax_service').send({
            url: '/admin/parts/save',
            method: 'post',
            data: data
        }, function(data){
            onSave(data.model);    
        });
    };

    s.import = function(files, eventObj){
        var formData = new FormData();
        for (var i = 0; i < files.length; i++) {
          formData.append('file', files[i]);
        }

        var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener("progress", eventObj.uploadProgress, false);
        xhr.addEventListener("load", eventObj.uploadComplete, false);
        xhr.addEventListener("error", eventObj.uploadFailed, false);
        xhr.addEventListener("abort", eventObj.uploadCanceled, false);
        xhr.open("POST", "/admin/parts/import");
        xhr.send(formData);
    };
            
    return s;
}; 
