/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var _fn = function($scope,$element,$attrs, $injector, $service){
    console.log('app.auth.controllers.SignIn');

    $scope.data={};
    
    $scope.form={};
    $scope.form.send=function(){
        $service.signin($scope.data);
        console.log("app.auth.controllers.SignIn.signin");
    };
}; 
    
module.exports = _fn;
