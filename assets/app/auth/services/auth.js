/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */    
var _fn = function($http, $injector){

    var service={};    

    service.signup=function(userData){
        $injector.get('app.common.ajax_service').send({
            url: '/signup',
            method: 'post',
            data: {user: userData}
        }, function(data){
            window.location.href='/';
        });
    };
    
    service.signin=function(userData){
        $injector.get('app.common.ajax_service').send({
            url: '/signin',
            method: 'post',
            data: {user: userData}
        }, function(data){
            window.location.href='/';
        });
    };

    return service;
};

module.exports = _fn;
