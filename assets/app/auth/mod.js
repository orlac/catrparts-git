/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cssify = require('cssify')
var authService = require('./services/auth.js');
var signUpCtrl = require('./controllers/SignUp.js');
var signInCtrl = require('./controllers/SignIn.js');

require('angular-loading-bar');
cssify.byUrl('/bower_components/angular-loading-bar/build/loading-bar.min.css');

require('angular-animate');

var init = function(){
        


    var mod = angular.module('auth', ['ngAnimate', 'angular-loading-bar']);
    var _requires=[];
    
    /** service */
    mod.factory('app.auth.services.auth', ['$http', '$injector',   function($http, $i){
            return new authService($http, $i);
    }] );
    mod.requires=mod.requires.concat( authService.requires || [] );
    
    
    
    
    /** controllers */

    mod.controller('app.auth.controllers.SignIn', [
        '$scope'
        ,'$element'
        ,'$attrs'
        ,'$injector'
        ,'app.auth.services.auth'
        ,signInCtrl ]);
    mod.requires=mod.requires.concat( signInCtrl.requires || [] );
    
    mod.controller('app.auth.controllers.SignUp', [
        '$scope'
        ,'$element'
        ,'$attrs'
        ,'$injector'
        ,'app.auth.services.auth'
        ,signUpCtrl ]);
    mod.requires=mod.requires.concat( signUpCtrl.requires || [] );
    
    return mod;
};



module.exports = {
    init: init
};

