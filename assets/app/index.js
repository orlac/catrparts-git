var cssify = require('cssify');
var $ = require('jquery');
window.jQuery = $;
window.$ = $;
var bs = require('bootstrap');
require('angular');
require('ui-bootstrap-tpls');


require('angular-animate');
require('angular-loading-bar');
require('angular-ui-router');
cssify.byUrl('/bower_components/angular-loading-bar/build/loading-bar.min.css');

var templateFactory = require('./common/services/templateFactory.js');
var rndBg = require('./common/services/rndBg.js');
var router = require('./index/router.js');

//var auth = require('./auth/mod.js');

var init = require('./common/initApp.js');

var apps = [
	'ui.router'
	,require('./index/navigate/module.js').init().name	
	,require('./auth/mod.js').init().name
];

init.app.requires = init.app.requires.concat(apps);

//todo в отдельный файл общие директивы и тд.
init.app.factory('templateFactory', [templateFactory]);
init.app.factory('rndBg', [rndBg]);

//main router
init.app.config( ['$stateProvider', '$urlRouterProvider', 
	function($stateProvider, $urlRouterProvider){
    	return new router($stateProvider, $urlRouterProvider);
} ] ); 


init.start();