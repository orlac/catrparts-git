return {
      "jquery": {
          "exports": "$"
      },
      "bootstrap": {
        "exports": "bootstrap",
        "depends": ["jquery:$"]
      },
      "notify-custom": {
        "exports": "null",
        "depends": ["$"]
      },
      "angular": {
          "exports": "angular",
          "depends": "jquery"
      },
      "angular-ui-tree": {
          "depends": ["angular"],
          "exports": "null"
      },
      "angular-animate": {
          "depends": ["angular"],
          "exports": "null"
      },
      "angular-loading-bar": {
          "depends": ["angular"],
          "exports": "null"
      },
      "ui-bootstrap-tpls": {
          "depends": ["angular", "bootstrap"],
          "exports": "null"
      }
  }