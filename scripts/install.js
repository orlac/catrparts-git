var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var path = require('path');
var url = require('url');

//console.log(process.env);

function doStdOut(data) {
  process.stdout.write(data);
}
function doStdErr(data) {
  process.stderr.write(data);
}


function grunt(next) {
	console.log('grunt install');
	exec('npm install grunt-cli -g', function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    if(stderr){
	    	console.log('stderr: ' + stderr);	
	    }
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }else{
	    	next();
	    }
	});

}

function browserify(next) {
  console.log('browserify install');
  exec('npm install browserify -g', function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    if(stderr){
	    	console.log('stderr: ' + stderr);	
	    }
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }else{
	    	next();
	    }
	});
}

function bower(next) {
  console.log('bower install');
  exec('npm install bower -g', function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    if(stderr){
	    	console.log('stderr: ' + stderr);	
	    }
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }else{
	    	next();
	    }
	});
}

function sails(next) {
  console.log('bower install');
  exec('sudo npm install sails -g', function (error, stdout, stderr) {
	    console.log('stdout: ' + stdout);
	    if(stderr){
	    	console.log('stderr: ' + stderr);	
	    }
	    if (error !== null) {
	      console.log('exec error: ' + error);
	    }else{
	    	next();
	    }
	});
}


var tasks = [grunt, browserify, bower, sails];

function doTask() {
  if (tasks.length) {
    var t = tasks.shift();
    t(doTask);
  }
  else {
    console.log('\ninstallation went fine, now `npm start`');
  }
}
doTask();